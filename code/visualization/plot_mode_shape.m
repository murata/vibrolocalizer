function [h] = plot_mode_shape(sim, k)
    tri = delaunay(sim.plate.plate.xr, sim.plate.plate.yr);
    h = figure('visible', 'off')
    mode_k = sim.plate.mode_shapes(3:3:end,k)/norm(sim.plate.mode_shapes(:,k));
    trisurf(tri, sim.plate.plate.xr, sim.plate.plate.yr, mode_k);
    lighting phong; shading interp; colorbar; colormap jet;
    title(['Mode No: ',num2str(k),' (',num2str(sim.plate.mode_frequencies(k)),' Hz)']);
end

