function [h] = floor_vibration_timeseries2(sim, k)
    % xr = X(:);
    % yr = Y(:);

    % Dispz = cos(pi.*xr)+sin(2.*pi.*yr);
    % size(Dispz)
    % size(squeeze(sim.signals.raw.displacement(:, :,  k))) 
    x = sim.plate.plate.xr;
    y = sim.plate.plate.yr;
    z = squeeze(sim.signals.raw.displacement(:, :,  k))';

    InterpFunc = scatteredInterpolant(x, y, z);
    [X,Y] = meshgrid(linspace(0, 1, 1000), linspace(0, 1, 1000));

    Z = InterpFunc(X,Y); % generate z points at desired trapezoid mesh

    h = figure;
    contourf(X,Y,Z, 25,'linestyle','none')

    fmt_string = sprintf('t=%2.2d [sec]', sim.t(k));
    title(fmt_string); 
end

