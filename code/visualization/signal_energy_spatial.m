function [h] = signal_energy_spatial(sim, impact_idx)
    % [xr, yr] = meshgrid(linspace(-1, 2, 1000), linspace(-1, 2, 100));
    tri = delaunay(sim.plate.plate.xr(:), sim.plate.plate.yr(:));
    % tri = delaunay(xr(:), yr(:));
    x = sim.plate.plate.xr(:);
    y = sim.plate.plate.yr(:);
    z = 10*log10(sim.signals.raw.energy(impact_idx, :))';
    % z = sim.signals.raw.energy(impact_idx, :)';
    % z = sim.signals.raw.displacement(impact_idx, :, 250)';
    tr = triangulation(tri, x, y, z);
    h = figure(impact_idx);
    trisurf(tr);
    lighting phong; shading interp; hold on;
    scatter3(x, y, z, 'r.');
end

