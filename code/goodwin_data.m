clc; clearvars; close all; prepare_generic;
load("data/goodwin_step_data.mat");
sym_g = load("data/del.mat");
g = sym_g.del;
f = sym_g.J;
h = sym_g.H;
L = experiment1.sensor_locations;
options = optimoptions('fminunc', ...
                       'Algorithm','trust-region', ...
                       'SpecifyObjectiveGradient',true, ...
                       'HessianFcn', 'objective', ...
                       'display', 'notify-detailed', ...
                       'MaxIterations', 250, ...
                       'StepTolerance', 1e-5, ...
                       'FunctionTolerance', 1e-5);
x_heur = zeros(experiment1.nsteps, 2);

parfor test_case=1:experiment1.nsteps
    logging('Case: %s is starting...', test_case);
    fun = @(x) fun_extra(x, experiment1.steps(test_case).e, experiment1.steps(test_case).nsamples, 1/sum(experiment1.steps(test_case).e), experiment1.sigma_zeta*ones(experiment1.nsensors, 1), L, f, g, h, false);
    [p_opt, fval, flag, output] = fminunc(fun, experiment1.steps(test_case).e, options);
    optim_results(test_case, :) = p_opt;
    et = p_opt(1:experiment1.nsensors);
    cov_zeta = diag(experiment1.sigma_zeta^2*ones(experiment1.nsensors, 1));
    K = diag([1/sum(et), 1/sum(et)]);
    mean_e = et + experiment1.steps(test_case).nsamples * diag(cov_zeta)';
    cov_e = 4 * diag(et) * cov_zeta' + 2 * experiment1.steps(test_case).nsamples * (cov_zeta * cov_zeta');
    cov_x = K * L * cov_e * L' * K';
    location_estimates(test_case, :) = K * L * transpose(mean_e);
    x_heur(test_case, :) = experiment1.steps(test_case).e * L' / sum(experiment1.steps(test_case).e);
end

e_semi = location_estimates - transpose(experiment1.groundtruth);
e_semi_norm = vecnorm(e_semi, 2, 2);
describe(e_semi_norm)
e_heur = x_heur - transpose(experiment1.groundtruth);
e_heur_norm = vecnorm(e_heur, 2, 2);
describe(e_heur_norm)
