derivation; 
clearvars; close all; clc; prepare_generic;
fun = load("data/del.mat");
experiment = load("data/katsidimas_etal_organized.mat");
test_case = 10;
net1 = 3;
net2 = 3;
nk = 2;
et_1 = linspace(experiment.e(test_case, 1) - 40 * experiment.nsamples *  experiment.var_zeta(test_case, 1), experiment.e(test_case, 1) + 50 * experiment.nsamples * experiment.var_zeta(test_case, 1), net1); 
et_2 = linspace(experiment.e(test_case, 2)- 60 * experiment.nsamples *  experiment.var_zeta(test_case, 2), experiment.e(test_case, 2) + 60 * experiment.nsamples * experiment.var_zeta(test_case, 2), net2); 
et_3 = 0;
et_4 = 0;

k = linspace(1e-6, 0.2, nk);

[a, b, c, d, e] = ndgrid(et_1, et_2, et_3, et_4, k); 
params = [a(:), b(:), c(:), d(:), e(:)];
J = zeros(length(params), 2);

parfor idx=1:length(params)
    tic;
    J_eval = subs(fun.J_inner, {"n", "e1", "e2", "e3", "e4", ...
                                "e_t1", "e_t2", "e_t3", "e_t4", "k", ...
                                "sigma_zeta1", "sigma_zeta2", "sigma_zeta3", "sigma_zeta4", ...
                                "x1", "x2", "x3", "x4", ...
                                "y1", "y2", "y3", "y4"}, ...
                                {experiment.nsamples, experiment.e(test_case, 1), experiment.e(test_case, 2), experiment.e(test_case, 3), experiment.e(test_case, 4), ...
                                params(idx, 1), params(idx, 2), params(idx, 3), params(idx, 4), params(idx, 5), ...
                                sqrt(experiment.var_zeta(test_case, 1)), sqrt(experiment.var_zeta(test_case, 2)), sqrt(experiment.var_zeta(test_case, 3)), sqrt(experiment.var_zeta(test_case, 4)), ...
                                experiment.sensor_locations(1, 1), experiment.sensor_locations(2, 1), experiment.sensor_locations(3, 1), experiment.sensor_locations(4, 1), ...
                                experiment.sensor_locations(1, 2), experiment.sensor_locations(2, 2), experiment.sensor_locations(3, 2), experiment.sensor_locations(4, 2)});
    J(idx, :) = double(J_eval);
    toc;
end
J = reshape(J, net1, net2, nk, 2);
J1 = squeeze(J(:, :, :, 1));
J2 = squeeze(J(:, :, :, 2));

% subplot(334);
% contourf(squeeze(a(:, 1, :)), squeeze(e(:, 1, :)), squeeze(J1(:, 1, :)), 'edgecolor', 'none');
% subplot(335);
% contourf(squeeze(a(:, 1, :)), squeeze(e(:, 1, :)), squeeze(J2(:, 1, :)), 'edgecolor', 'none');
% subplot(336);
% contourf(squeeze(a(:, 1, :)), squeeze(e(:, 1, :)), squeeze(J1(:, 1, :)) + squeeze(J2(:, 1, :)), 'edgecolor', 'none');

% subplot(337);
% contourf(squeeze(b(1, :, :)), squeeze(e(1, :, :)), squeeze(J1(1, :, :)), 'edgecolor', 'none');
% subplot(338);
% contourf(squeeze(b(1, :, :)), squeeze(e(1, :, :)), squeeze(J2(1, :, :)), 'edgecolor', 'none');
% subplot(339);
% contourf(squeeze(b(1, :, :)), squeeze(e(1, :, :)), squeeze(J1(1, :, :)) + squeeze(J2(1, :, :)), 'edgecolor', 'none');
