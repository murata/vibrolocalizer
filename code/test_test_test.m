qq = subs(cov_x, {"n", "e_t1", "e_t2",  "sigma_zeta1", "sigma_zeta2",  "x1", "x2",  "y1", "y2", "k"}, ....
                    {n, params(i, 2), params(i, 2), sigma_zeta(1), sigma_zeta(2),  L(1, 1), L(1, 2),  L(2, 1), L(2, 2), 1e-12});
double(qq)
