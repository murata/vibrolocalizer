prepare_evaluation;
%%
load('data/deterministic/sim.mat');

xr = plate.xrCoordinates;
yr = plate.yrCoordinates;
signals = ["043", "060", "095", "115", "127", "134", "141", "166", "191", "209", "211", "228", "232", "242", "257", "263", "264", "268", "276", "282", "285", "293", "298", "305", "312", "315", "323", "325", "364", "391"]

for signal=signals
    fpath = "data/deterministic/signals_" + signal + ".mat"
    load(fpath);
    impact_id = str2num(signal); sensor_id = 1:456;
    e_all = energy(acc(:, 3*sensor_id)');
    e_0 = energy(acc(:, 3*impact_id)');

    d_all = vecnorm(([xr(sensor_id), yr(sensor_id)] - [xr(impact_id), yr(impact_id)])')';

    [gamma_hat, res_d] = estimate_gamma(e_all, e_0, d_all);
    idx = isfinite(gamma_hat);
    % consider only the valid ones
    sensor_id = sensor_id(idx); gamma_hat = gamma_hat(idx);
    [mean(gamma_hat), median(gamma_hat), std(gamma_hat), mad(gamma_hat, 1)]
end 
%% plots
% figure(1);
% subplot(3, 3, 1:3);
% tri = delaunay(xr(sensor_id),yr(sensor_id));
% trisurf(tri, xr(sensor_id), yr(sensor_id), gamma_hat);
% lighting phong;
% shading interp;
% colorbar;
% colormap(jet);
% axis square;
% grid on; grid minor;
% xlabel('$x$');
% ylabel('$y$');
% zlabel('$\hat{\gamma}$');
% title('Spatial Distribution');

% subplot(3, 3, 4:6);
% histogram(gamma_hat, 150, 'Normalization', 'pdf'); hold on;
% p = mle(abs(gamma_hat), 'distribution', 'rayl');
% qq = raylinv([0.0001, .9999], p);
% qq = linspace(min(qq), max(qq), 1000);
% plot(-qq, raylpdf(qq, p));


% grid on; grid minor;
% xlabel('$\gamma$');
% ylabel('$f_{\Gamma}\left(\gamma \right)$');
% title('Empirical Probability Density Function');

% subplot(3, 3, 7:9);
% histogram(abs(res_d), linspace(0, 0.3, 1000));
% grid on; grid minor;
% title('Empirical Probability Density Function');
% xlabel('Distance Residuals $r_d$');
% ylabel('$f_{R_d}\left(r_d \right)$');
