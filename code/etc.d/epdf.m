function [x, fx] = epdf(varargin)
    [fx, x] = histcounts(varargin{:}, 'Normalization', 'pdf');
    fx(end+1) = 0;
end
