function [mu_lognormal,sigma_lognormal] = lognormal_moments_from_normal(mu_normal, sigma_normal)
    mu_lognormal = exp(mu_normal + sigma_normal);
    sigma_lognormal = sqrt(exp(sigma_normal.^2)-1).*sqrt(exp(2*mu_normal+sigma_normal));
end