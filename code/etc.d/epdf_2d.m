function [x, y, fxy] = epdf_2d(varargin)
    [fxy, x, y] = histcounts2(varargin{:}, 'Normalization', 'pdf');
    fxy(end+1, end+1) = 0;
end