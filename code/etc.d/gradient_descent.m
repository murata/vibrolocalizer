function [p_next, cost, p_hist] = gradient_descent(F, G, J_G, p, niter, eta, gamma, ftol, xtol, gtol)
% Gradient descent algorithm to solve PEBLE self-calibration problem.
%
% F: the cost function (scalar-valued and multivariate) to minimize 
% usually is in  the form of 0.5 * G' * G 
% G: the original multivariate vector-valued function 
% p: initial point for search
% niter: number of iterations allowed
% eta: step length
% gamma: Nesterov Accelerated Gradient parameter (keep it around 0.9)
% ftol: stopping criteria for cost
% xtol: stopping criteria for minimum change in the solution
% gtol: stopping criteria for the change in the cost between two
% consecutive iterations
cost_prev = F(p);
cost = cost_prev;
dx_prev = 0;
p_next = p;
p_hist = [p];
for i=1:niter
    dx = gamma *  dx_prev + eta * J_G(p_next)' * G(p_next);
    dx_prev = dx;
    
%     if dx < xtol
%         disp(['Converged (xtol): ', num2str(xtol)]);
%         break;
%     end
    p_next = p_next - dx;
    p_hist = [p_hist, p_next];
    cost = F(p_next);
    disp(['Iteration: ', num2str(i), ' cost: ', num2str(cost), ' ', num2str(G(p_next)')]);
    if cost < ftol
        disp(['Converged (ftol): ', num2str(cost)]);
        break
    end
    if isinf(cost)
        error('gradientdescent:costinf', 'Failed cost is inf!');
    end
     if isnan(cost)
        error('gradientdescent:costnan', 'Failed cost is nan!');
    end
    dcost = abs(cost_prev - cost);
    if dcost < gtol
        disp(['Converged (gtol): ', num2str(dcost)]);
        break
    end
    cost_prev = cost;
end
if i==niter
    disp('Probably not converged');
end
end

