function varargout = describe(varargin)
    if nargin == 1
        error = varargin{1};
        thr = inf;
        scaler = 1;
    elseif nargin == 2
        error = varargin{1};
        thr = varargin{2};
        scaler = 1;
    elseif nargin == 3
        error = varargin{1};
        thr = varargin{2};
        scaler = varargin{3};
    end
    error = error * scaler;
    error(error>thr) = [];
    mean_e = mean(error, 'omitnan');
    median_e = median(error, 'omitnan');
    std_e = std(error, 'omitnan');
    rms_e = rms(error, 'omitnan');
    min_e = min(error);
    max_e = max(error);
    description = [mean_e, std_e, median_e, rms_e, min_e, max_e];

    if nargout == 0
        fprintf("Mean: %2.2f, Std. Dev: %2.2f, Median: %2.2f, RMS: %2.2f, Min: %2.2f, Max: %2.2f\n", description);
    elseif nargout == 1
        varargout{1} = description;
    end
end
