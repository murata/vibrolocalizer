function E = energy(x, dim)
E = sum(x.^2, dim);
end

