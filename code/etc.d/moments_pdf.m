function moments = moments_pdf(x, fx, n)
    moments = zeros(n, 1);
    mean = trapz(x, x.*fx);
    moments(1) = mean;
    for i=2:length(moments)
        moments(i) = trapz(x, (x-mean).^i.*fx);
    end
end

