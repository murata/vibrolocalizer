function pdf = norm_pdf(x, mu, sigma, c)
    pdf = 1/sqrt(2*c)./sigma.*exp(-0.5*(mu-x).^2/sigma^2);
end