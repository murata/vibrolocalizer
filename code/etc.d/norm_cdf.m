function cdf = norm_cdf(x, mu, sigma)
    cdf = 1/2+ 1/2*erf((x-mu)/(sqrt(2*sigma)));
end
