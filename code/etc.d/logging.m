function [formatted_message, bytes_console, bytes_file] = logging(message, variables, options)
    arguments
        message string
    end
    arguments(Repeating)
        variables (1, :)
    end
    arguments
        options.file = false
        options.path = "log.txt"
        options.mode = "a+"
        options.level = "DEBUG"
    end
    bytes_console = 0;
    bytes_file = 0;

    available_logging_levels = get_available_levels();
    assert(ismember(options.level, available_logging_levels), "Wrong level was activated");
    
    formatted_message = format_message(options.level, message, variables);
    bytes_console = print_console(formatted_message);
    if options.file == true
        bytes_file = print_file(formatted_message, options.path, options.mode);
    end
    formatted_message = erase(formatted_message, " " + newline);
end

function formatted_message = format_message(level, message, variables)
    arguments
        level
        message string
    end
    arguments(Repeating)
        variables (1, :) string
    end

    now = time();
    variables = variables{1};
    message = message{1};
    message_start = "[" + level + "] (" + datestr(now) + "): "; 
    message = join([message_start, message, "\n"]);
    escaped_percent = count(message, "%%");
    all_percent = count(message, "%");
    formatting_percent = all_percent - 2*escaped_percent;
    assert(formatting_percent == length(variables), "number of variables must be equal to number of formatters");
    formatted_message = sprintf(message, variables(:));
end

function bytes = print_console(message)
    bytes = fprintf("%s", message);
end

function bytes = print_file(message, file_path, file_mode)
    fd = fopen(file_path, file_mode);
    if fd == -1
        error('IO error file cannot be opened');
    end

    bytes = fprintf(fd, message);
    success_close = fclose(fd);

    if success_close == -1
        error('IO error file cannot be opened');
    end
end

function available_levels = get_available_levels()
    available_levels = ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"];
end

