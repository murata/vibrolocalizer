function fx = makepdf(x,fx)
    fx = fx / trapz(x, fx);
end

