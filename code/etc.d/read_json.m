function data = readjson(fname)
    fid = fopen(fname, 'r'); 
    if fid == -1
        error("File was not opened!");
    end
    raw = fread(fid,inf); 
    str = char(raw'); 
    fclose(fid); 
    data = jsondecode(str);
end
