function [mu_normal, sigma_normal] = normal_moments_from_lognormal(mu_lognormal, sigma_lognormal)
    mu_normal = log(mu_lognormal.^2./sqrt(mu_lognormal.^2 + sigma_lognormal.^2));
    sigma_normal = log(1+sigma_lognormal.^2./mu_lognormal.^2);
end