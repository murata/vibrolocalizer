function [gamma_hat, res_d] = estimate_gamma(e_all, e_0, d_all)
    gamma_hat = log(e_all./e_0)./ d_all;
    d_hat = 1./gamma_hat .* log(e_all/e_0)';
    res_d = evaluate(d_all, d_hat);
end

