mu = [0 0];
S = [1 0; 0 3];
x = linspace(-10, 10, 100);
y = linspace(-10, 10, 100);
X = [x', y'];
y = mvnpdf(X,mu,S);
x_in = [0; 4];
x_out = [-1; 3];
d_in = mahal_dist(x_in, mu', S)
scatter(x_in(1), x_in(2), 'rx'); hold on;
scatter(x_out(1), x_out(2), 'gx');

error_ellipse('mu', mu, 'C', S, 'conf', .95);
axis equal; xlim([-10, 10]); ylim([-10, 10]);
grid on; grid minor;


function d = mahal_dist(x, mu, sigma)
    d = sqrt( (x-mu)' * inv(sigma) * (x-mu));
end
