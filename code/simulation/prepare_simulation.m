%% Prepare Deterministic Dynamic Simulations
logging("Parsing configuration...");
config_simulation;
%%
logging("Starting the prep work...");
start_time = time();
mkdir(sim.output.folder);
% Generate plate and state space representation for the dynamics
logging("Generating the plate...");
plate = generate_plate(sim.plate.boundary_conditions, sim.plate.nw, sim.plate.nl, sim.plate.plate_width, sim.plate.plate_length, sim.plate.plate_thickness, sim.plate.rho, sim.plate.E, sim.plate.v, sim.plate.C);
sim.plate.plate = plate;
sim.dof = plate.dof;
sim.nnodes = plate.nnode;
logging("Calculating the mode shapes...");
[mode_shapes, mode_frequencies] = plate_mode_shapes(plate.K, plate.M);
sim.plate.mode_shapes = mode_shapes;
sim.plate.mode_frequencies = mode_frequencies;
logging("Mode shapes are done...");

logging("Selecting sensors...");
[sensor_idx, sensor_locations] = select_sensors(sim.nsensors, sim.plate.center, sim.diameter_sensor,  plate.xr, plate.yr, sim.sensor_selection);
sim.sensor_idx = sensor_idx;
sim.sensor_locations = sensor_locations;
sim.nsensors = length(sensor_idx);
logging("Choosing impact locations...");
[impact_idx, impact_locations] = choose_impact_locations(sim.nlocations, sim.plate.center, sim.diameter_impact, plate.xr, plate.yr, sensor_idx, sim.impact_location_selection);
sim.impact_idx = impact_idx;
sim.impact_locations = impact_locations;
sim.nlocations = size(impact_locations, 1);
% Generate forcing and sensor location
logging("Generating the forcing...");
[u, t] = generate_forcing_and_time(sim.fs, sim.nsteps, 0.1, 0.6, 0.425, 0.05, 0.4, 0.05, sim.forcing_type);
sim.t = t;
sim.forcing = u;
sim.nsamples = length(t);
sim.signals.raw.acceleration = zeros(sim.nlocations, sim.nsensors, sim.nsamples);
% sim.signals.raw.velocity = zeros(sim.nlocations, sim.nsensors, sim.nsamples);
% sim.signals.raw.displacement = zeros(sim.nlocations, sim.nsensors, sim.nsamples);

% sim.signals.sensor.acceleration = zeros(sim.nrepeat, sim.nlocations, sim.nsensors, sim.nsamples);
% sim.signals.sensor.velocity = zeros(sim.nrepeat, sim.nlocations, sim.nsensors, sim.nsamples);
% sim.signals.sensor.displacement = zeros(sim.nrepeat, sim.nlocations, sim.nsensors, sim.nsamples);
sim.signals.sensor.energy = zeros(sim.nrepeat, sim.nlocations, sim.nsensors);

sim.sim_duration = seconds(zeros(sim.nlocations, 1));
sim.simulation_completed = false([sim.nlocations, 1]);

q = repmat(sim.impact_locations, [1, 1, sim.nrepeat]);
q=permute(q, [3, 1, 2]);
sim.signals.groundtruth.threed = zeros(sim.nrepeat, sim.nlocations, 3);
sim.signals.groundtruth.threed(:, :, 1:2) = q;
logging("Generating state space representation...");
[state_space, state_space_discrete, initial_state] = generate_state_space(sim.plate.plate, impact_idx, sim.nsteps, sim.Ts, "foh");
sim.state_space = state_space;
sim.state_space_discrete = state_space_discrete;
sim.initial_state = initial_state;
% save_sim(sim);
telapsed = time() - start_time;
logging("Prep work done in %2.2f [seconds]", seconds(telapsed));
clearvars -except sim;
