function [u, t] = generate_forcing_and_time(fs, nstep, ...
                                            first_impact_time, stance_time, ...
                                            mean_swing, std_swing, ...
                                            mean_shift, std_shift, type) 
    Ts = fs^-1;
    if type == "grf"                                     
        times = generate_step_timing(first_impact_time, nstep, ...
                                    stance_time, mean_swing, std_swing, ...
                                    mean_shift, std_shift);
        tmax = ceil(max(times))+1;
        t=0:Ts:tmax;
        u = zeros(length(t), nstep);
        
        load('data/healthy_GRFS.mat', 'healthy_GRFS');
        sig = mean(healthy_GRFS, 2);
        
        fstep = ceil(length(sig) / stance_time); % Hz
        
        sig_resampled = resample(sig, fs, fstep);
        
        for i=1:length(times)
            [~, t_id] = min(abs(t-times(i)));
            u(t_id:t_id+length(sig_resampled)-1, i) = sig_resampled;
        end
    else
        t=0:Ts:0.4;
        u = zeros(size(t));
        [~, t_id] = min(abs(t-0.1));

        u(t_id) = 8*9.81;
    end
end

