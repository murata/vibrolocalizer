function [node_idx,node_loc] = choose_impact_locations(n, center, d, xnode, ynode, sensor_idx, method)
    if method == "random"
        xy_node = [xnode, ynode];
        r = vecnorm(xy_node-center, 2, 2);
        available_node_idx = find(r<d);
        node_idx = randsample(length(available_node_idx),n);
        % navailable_nodes = length(available_node_idx);
        % impact location should not coincide with the sensor location
        % assert(sum(ismember(node_idx, sensor_idx)) == 0, "Please change the seed, impact locations coincide with sensor locations");
        node_idx = available_node_idx(node_idx);
    else
        node_idx = 1:length(xnode);
    end
    node_idx = sort(node_idx)';
    node_loc = [xnode(node_idx), ynode(node_idx)];
end

