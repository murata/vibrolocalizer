function step_locations = generate_step_location(start, nstep, ...
                                                 mean_length, std_length, ...
                                                 mean_width, std_width)
                                              
step_width = mean_width + randn(nstep, 1) * std_width;
step_length = mean_length + randn(nstep, 1) * std_length;

step_locations = zeros(nstep, 2); % n steps in the room
% assign the first step location manually
step_locations(1, 1) = start(1) + step_length(1);
step_locations(1, 2)= start(2) - step_width(1);
% calculate the rest of them.
for i=2:nstep
    step_locations(i, 1) = step_locations(i-1, 1) + step_length(i);
    step_locations(i, 2) = step_locations(i-1, 2) + (-1)^(i) * step_width(i);
end

end

