%%
sim = struct();
sim.continue_simulation = true;
sim.force_write = true;
sim.plots = false;
sim.parallel = false;                                       % parallelize simulation
sim.seed = 31311631;                                        % rng seed, good for repeatability
sim.diameter_impact = 0.2;                                  % [meters]
sim.diameter_sensor = 0.3;                                  % [meters]
%% Simulation parameters
% generic stuff: sampling frequency, wall time, etc.
sim.fs = 2^15;                                              % sampling frequency
sim.Ts = 1/sim.fs;                                          % sampling period
sim.nsteps = 1;                                             % number of steps in each simulation case
sim.nrepeat = 500;                                          % number of sensor configurations
sim.nlocations = 1;                                         % number of impacts to generate in total
sim.nsensors = 4;                                           % number of sensors to simulate
sim.variances = 2;
sim.sensor_selection = "all";                               % all or random
sim.forcing_type = "impulse";                               % grf or impulse
sim.impact_location_selection = "random";                      % all or random
sim.training_ratio = 0.1;
% Plate material properties
sim.plate.center = [0.5, 0.5];                                  % center of the floor
sim.plate.rho=2443;
sim.plate.E=31.49e9;
sim.plate.v=0.2;
sim.plate.material='GWC';
sim.plate.nw=18; sim.plate.nl=18;                          % number of elements in each direction
sim.plate.boundary_conditions='Fixed';                     % Select BCs 'Free' or 'Fixed'
sim.plate.plate_width=1;                                   % plate dimensioons [meters]
sim.plate.plate_length=1;
sim.plate.plate_thickness=0.025;
sim.plate.C='1e2*(M+7.2e-8*K)';                            % damping matrix

sim.output.folder = 'data/';
sim.output.filename = sprintf('data/energy_all-%d-%d-%dk.mat', sim.plate.nw, sim.plate.nl, log2(sim.fs)) ;

rng(sim.seed);

clearvars -except sim;
