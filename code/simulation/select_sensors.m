function [sensor_idx,sensor_loc] = select_sensors(n, center, d, xnode, ynode, choice)
    theta = linspace(0, 2*pi, n+1);
    theta = theta(1:end-1);
    xs = center(1) + d*cos(theta);
    ys = center(2) + d*sin(theta);
    if choice == "all"
        sensor_idx = 1:length(xnode);
        sensor_loc = [xnode, ynode];
    else
        [sensor_idx, sensor_loc] = locate_closest_node(xs', ys', xnode, ynode);
    end
end

