function space = generate_space(xmin, xmax, ymin, ymax, nx, ny)
    x = linspace(xmin, xmax, nx);
    y = linspace(ymin, ymax, ny);

    [xx, yy] = meshgrid(x, y);
    space.x = x;
    space.y = y;
    space.xx = xx;
    space.yy = yy;
end

