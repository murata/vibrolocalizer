function dt = save_sim(sim)
    tstart = time();
    % save resulting struct to a file
    save(sim.output.filename, "sim", "-v7.3", "-nocompression")
    dt = time() - tstart;
    logging("IO work done in %2.2f [seconds]", seconds(dt));
    file_size = dir(sim.output.filename).bytes / 1024 / 1024 / 1024;
    memory_size = whos("sim").bytes / 1024 / 1024 / 1024;
    logging("Size: %2.2f [GB] (in memory) %2.2f [GB] (in disk)", memory_size, file_size);
end
