function [f, g, h] = fun_extra(x, e, n, k, sigma_zeta, L, f, g, h, plot)
    m = length(sigma_zeta);
    e_t = x(1:m);
    K = diag([k, k]);
    cov_zeta = diag(sigma_zeta.^2);
    mu_e = e_t + n * diag(cov_zeta)';
    cov_e = (4 * diag(e_t) + 2 * n * cov_zeta) * cov_zeta;
    mu_x = K * L * mu_e';
    cov_x = K * L * cov_e * L' * K';
    f = subs(f, {"n", ...
                "e1", "e2", "e3", "e4", "e5", "e6", "e7", "e8", "e9", "e10", "e11", ...
                "e_t1", "e_t2", "e_t3", "e_t4", "e_t5", "e_t6", "e_t7", "e_t8", "e_t9", "e_t10", "e_t11", ...
                "sigma_zeta1", "sigma_zeta2", "sigma_zeta3", "sigma_zeta4", "sigma_zeta5", "sigma_zeta6", "sigma_zeta7", "sigma_zeta8", "sigma_zeta9", "sigma_zeta10", "sigma_zeta11", ...
                "x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8", "x9", "x10", "x11", ...
                "y1", "y2", "y3", "y4", "y5", "y6", "y7", "y8", "y9", "y10", "y11", ...
                }, ...
                {n, ...
                e(1), e(2), e(3), e(4), e(5), e(6), e(7), e(8), e(9), e(10), e(11), ...
                x(1), x(2), x(3), x(4), x(5), x(6), x(7), x(8), x(9), x(10), x(11), ...
                sigma_zeta(1), sigma_zeta(2), sigma_zeta(3), sigma_zeta(4), sigma_zeta(5), sigma_zeta(6), sigma_zeta(7), sigma_zeta(8), sigma_zeta(9), sigma_zeta(10), sigma_zeta(11), ...
                L(1, 1), L(1, 2), L(1, 3), L(1, 4), L(1, 5), L(1, 6), L(1, 7), L(1, 8), L(1, 9), L(1, 10), L(1, 11), ...
                L(2, 1), L(2, 2), L(2, 3), L(2, 4), L(2, 5), L(2, 6), L(2, 7), L(2, 8), L(2, 9), L(2, 10),  L(2, 11)});
    f = double(f);
    g = subs(g, {"n", ...
                 "e1", "e2", "e3", "e4", "e5", "e6", "e7", "e8", "e9", "e10", "e11", ...
                 "e_t1", "e_t2", "e_t3", "e_t4", "e_t5", "e_t6", "e_t7", "e_t8", "e_t9", "e_t10", "e_t11", ...
                 "sigma_zeta1", "sigma_zeta2", "sigma_zeta3", "sigma_zeta4", "sigma_zeta5", "sigma_zeta6", "sigma_zeta7", "sigma_zeta8", "sigma_zeta9", "sigma_zeta10", "sigma_zeta11", ...
                 "x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8", "x9", "x10", "x11", ...
                 "y1", "y2", "y3", "y4", "y5", "y6", "y7", "y8", "y9", "y10", "y11", ...
                 }, ...
                 {n, ...
                 e(1), e(2), e(3), e(4), e(5), e(6), e(7), e(8), e(9), e(10), e(11), ...
                 x(1), x(2), x(3), x(4), x(5), x(6), x(7), x(8), x(9), x(10), x(11), ...
                 sigma_zeta(1), sigma_zeta(2), sigma_zeta(3), sigma_zeta(4), sigma_zeta(5), sigma_zeta(6), sigma_zeta(7), sigma_zeta(8), sigma_zeta(9), sigma_zeta(10), sigma_zeta(11)...
                 L(1, 1), L(1, 2), L(1, 3), L(1, 4), L(1, 5), L(1, 6), L(1, 7), L(1, 8), L(1, 9), L(1, 10), L(1, 11), ...
                 L(2, 1), L(2, 2), L(2, 3), L(2, 4), L(2, 5), L(2, 6), L(2, 7), L(2, 8), L(2, 9), L(2, 10),  L(2, 11)});

    g = double(g);
    h = subs(h, {"n", ...
                "e1", "e2", "e3", "e4", "e5", "e6", "e7", "e8", "e9", "e10", "e11", ...
                "e_t1", "e_t2", "e_t3", "e_t4", "e_t5", "e_t6", "e_t7", "e_t8", "e_t9", "e_t10", "e_t11", ...
                "sigma_zeta1", "sigma_zeta2", "sigma_zeta3", "sigma_zeta4", "sigma_zeta5", "sigma_zeta6", "sigma_zeta7", "sigma_zeta8", "sigma_zeta9", "sigma_zeta10", "sigma_zeta11", ...
                "x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8", "x9", "x10", "x11", ...
                "y1", "y2", "y3", "y4", "y5", "y6", "y7", "y8", "y9", "y10", "y11", ...
                }, ...
                {n, ...
                e(1), e(2), e(3), e(4), e(5), e(6), e(7), e(8), e(9), e(10), e(11), ...
                x(1), x(2), x(3), x(4), x(5), x(6), x(7), x(8), x(9), x(10), x(11), ...
                sigma_zeta(1), sigma_zeta(2), sigma_zeta(3), sigma_zeta(4), sigma_zeta(5), sigma_zeta(6), sigma_zeta(7), sigma_zeta(8), sigma_zeta(9), sigma_zeta(10), sigma_zeta(11), ...
                L(1, 1), L(1, 2), L(1, 3), L(1, 4), L(1, 5), L(1, 6), L(1, 7), L(1, 8), L(1, 9), L(1, 10), L(1, 11), ...
                L(2, 1), L(2, 2), L(2, 3), L(2, 4), L(2, 5), L(2, 6), L(2, 7), L(2, 8), L(2, 9), L(2, 10),  L(2, 11)});

    h = double(h);
    if plot
        figure(666);
        rectangle('Position', [0 0 60 60], 'EdgeColor', [0 0 0], 'FaceColor', [0.7 0.7 0.7]); hold on;
        scatter(L(1, :), L(2, :), 'kx');
        scatter(mu_x(1), mu_x(2), 'r.')
        title(num2str(round([mu_x', k_x, k_y], 2)))
        axis equal; grid on; grid minor;
        xlim([-10 70]); ylim([-10 70]); drawnow;
    end
end
