load('data/long_run.mat');
estimates = squeeze(mean(location_estimates, 2));
error_50 = vecnorm(estimates-exp.gt, 2, 2);
error_e = vecnorm(squeeze(location_estimates(:, 1, :))-exp.gt, 2, 2);
error_c = vecnorm(squeeze(location_estimates(:, 2, :))-exp.gt, 2, 2);
x_heur = exp.e * L' ./ sum(exp.e, 2);
error_heur = vecnorm(x_heur-exp.gt, 2, 2);

describe(error_50)
describe(error_heur)
[e_space, e_pdf] = epdf(error_50, 200);
[e_space_heur, e_pdf_heur] = epdf(error_heur, 200);
idx = error_50<10;
idx_heur = error_heur<10;
%%
figure(1);
plot(e_space, e_pdf, 'r-.', e_space_heur, e_pdf_heur, 'k-.'); hold on;
plot(e_space, cumtrapz(e_space, e_pdf), 'r-', e_space_heur, cumtrapz(e_space_heur, e_pdf_heur), 'k-'); 
grid on; grid minor;
% NOTE: this is an important figure
% scatter(1./squeeze(optim_results(:, 2, end)), sum(exp.e, 2))

figure(2);
subplot(121); scatter(exp.gt(idx, 1), exp.gt(idx, 2), 'gx'); hold on;
scatter(exp.gt(~idx, 1), exp.gt(~idx, 2), 'rx')
axis equal; xlim([-20 50]); ylim([-20 50]);
grid on; grid minor;

subplot(122); scatter(exp.gt(idx_heur, 1), exp.gt(idx_heur, 2), 'gx'); hold on;
scatter(exp.gt(~idx_heur, 1), exp.gt(~idx_heur, 2), 'rx')
axis equal; xlim([-20 50]); ylim([-20 50]);
grid on; grid minor;
