clc; clearvars; close all; prepare_generic;
experiment = load('data/katsidimas_etal_organized.mat');
sym_g = load("data/del_simplified.mat");
nbins = 100;
L = experiment.sensor_locations';
g = sym_g.del;
f = sym_g.J;
options = optimoptions('fminunc', ...
                       'Algorithm','trust-region', ...
                       'SpecifyObjectiveGradient',true, ...
                       'display', 'iter-detailed', ...
                       'MaxIterations', 100, ...
                       'StepTolerance', 1e-5, ...
                       'FunctionTolerance', 1e-5);
mu_x_all = zeros(experiment.nexperiments, 2, 2);
cov_x_all = zeros(experiment.nexperiments, 2, 2, 2);
optim_results = zeros(experiment.nexperiments, 2, 4);

for test_case =1:50 %experiment.nexperiments;
    weights = [0,1];
    fun = @(x) fun_extra(x, weights, experiment.e(test_case, :), experiment.nsamples, sqrt(experiment.var_zeta(test_case, :)), L, f, g, false);
    p0 = [experiment.e(test_case, :)];
    [p_opt, fval, flag, output] = fminunc(fun, p0, options);
    et = p_opt;
    cov_zeta = diag(experiment.var_zeta(test_case, :));
    mu_e = et' + experiment.nsamples * diag(cov_zeta);
    cov_e = 4 * diag(et) * cov_zeta' + 2 * experiment.nsamples * (cov_zeta * cov_zeta');
    mu_x = (L * mu_e)' / norm(et, 1);
    cov_x = L * cov_e * L' / norm(et, 1)^2;
    mu_x_all(test_case, 1, :) = mu_x;
    cov_x_all(test_case, 1, :, :) = cov_x;
    optim_results(test_case, 1, :) = et;

    weights = [1,0];
    fun = @(x) fun_extra(x, weights, experiment.e(test_case, :), experiment.nsamples, sqrt(experiment.var_zeta(test_case, :)), L, f, g, false);
    p0 = [experiment.e(test_case, :)];
    [p_opt, fval, flag, output] = fminunc(fun, p0, options);
    et = p_opt;
    cov_zeta = diag(experiment.var_zeta(test_case, :));
    mu_e = et' + experiment.nsamples * diag(cov_zeta);
    cov_e = 4 * diag(et) * cov_zeta' + 2 * experiment.nsamples * (cov_zeta * cov_zeta');
    mu_x = (L * mu_e)' / norm(et, 1);
    cov_x = L * cov_e * L' / norm(et, 1)^2;
    mu_x_all(test_case, 2, :) = mu_x;
    cov_x_all(test_case, 2, :, :) = cov_x;
    optim_results(test_case, 2, :) = et;
end

function [f, g] = fun_extra(x, lambda, e, n, sigma_zeta, L, f, g, plot)
    m = length(sigma_zeta);
    e_t = x(1:m);
    cov_zeta = diag(sigma_zeta.^2);
    mu_e = e_t + n * diag(cov_zeta)';
    cov_e = (4 * diag(e_t) + 2 * n * cov_zeta) * cov_zeta;
    mu_x = L*mu_e'/ norm(e_t, 1);
    cov_x = L * cov_e * L' / norm(e_t, 1)^2;

    f = subs(f, {"n", "e1", "e2", "e3", "e4", ...
                "e_t1", "e_t2", "e_t3", "e_t4", ...
                "sigma_zeta1", "sigma_zeta2", "sigma_zeta3", "sigma_zeta4", ...
                "lambda1", "lambda2", ...
                "x1", "x2", "x3", "x4", ...
                "y1", "y2", "y3", "y4"}, ...
                {n, e(1), e(2), e(3), e(4), ...
                e_t(1), e_t(2), e_t(3), e_t(4), ...
                sigma_zeta(1), sigma_zeta(2), sigma_zeta(3), sigma_zeta(4), ...
                lambda(1), lambda(2), ...
                L(1, 1), L(1, 2), L(1, 3), L(1, 4), ...
                L(2, 1), L(2, 2), L(2, 3), L(2, 4)});
    f = double(f);
    g = subs(g, {"n", "e1", "e2", "e3", "e4", ...
                "e_t1", "e_t2", "e_t3", "e_t4", ...
                "sigma_zeta1", "sigma_zeta2", "sigma_zeta3", "sigma_zeta4", ...
                "lambda1", "lambda2", ...
                "x1", "x2", "x3", "x4", ...
                "y1", "y2", "y3", "y4"}, ...
                {n, e(1), e(2), e(3), e(4), ...
                e_t(1), e_t(2), e_t(3), e_t(4), ...
                sigma_zeta(1), sigma_zeta(2), sigma_zeta(3), sigma_zeta(4), ...
                lambda(1), lambda(2), ...
                L(1, 1), L(1, 2), L(1, 3), L(1, 4), ...
                L(2, 1), L(2, 2), L(2, 3), L(2, 4)});

    g = double(g);
    if plot
        figure(666);
        rectangle('Position', [0 0 30 30], 'EdgeColor', [0 0 0], 'FaceColor', [0.7 0.7 0.7]); hold on;
        scatter(L(1, :), L(2, :), 'kx');
        scatter(mu_x(1), mu_x(2), 'r.')
        title(num2str(round([mu_x'], 2)))
        axis equal; grid on; grid minor;
        xlim([-10 40]); ylim([-10 40]); drawnow;
    end
end
