clc; clearvars; close all; prepare_generic;
m = 2;
syms e epsilon e_t sigma_zeta [m, 1] real;
syms n k k_x k_y real;
syms x y [m 1] real;
% syms K [m m] real;
syms lambda [2 1] real;
L = [x'; y'];
% assumeAlso(sum(lambda)==1);
assumeAlso(e>0);
assumeAlso(epsilon>0);
assumeAlso(e_t>=0);
assumeAlso(sigma_zeta>0);
assumeAlso(n>0);

cov_zeta = diag(sigma_zeta.^2);
mean_e = e_t + n * diag(cov_zeta);
cov_e = (4 * diag(e_t) + 2 * n * cov_zeta) * cov_zeta';
cov_e_app = 2 * n * (cov_zeta * cov_zeta');

% mean_epsilon = n * ones(m, 1);
% cov_epsilon = n * diag(sigma_zeta.^4) + 4 *  n * diag(sigma_zeta.^2) * diag(e-epsilon);
K = diag([k_x k_y]); 

% mean_x  = L * mean_e / sum(e_t);
% cov_x = L * cov_e * L' / sum(e_t)^2;

mean_x  = K * L * mean_e;
cov_x = K * L * cov_e_app * L' * K';

J_inner = simplify([(e-mean_e)'*inv(cov_e)*(e-mean_e);
                    det(cov_x)]);
J = lambda' * J_inner;
%% 
p = [e_t; k_x; k_y];
del = gradient(J, p);
del = simplify_auto(del, false, 1000);
sol = solve(del, p, 'Real', true, 'ReturnConditions', true)
% save("data/del.mat", "del", "J");

