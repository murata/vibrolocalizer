clc; clearvars; close all; prepare_generic;
exp = load('data/katsidimas_etal_organized.mat');
sym_g = load("data/del.mat");
g = sym_g.del;
f = sym_g.J;
h = sym_g.H;
L = exp.sensor_locations';
options = optimoptions('fminunc', ...
                       'Algorithm','trust-region', ...
                       'SpecifyObjectiveGradient',true, ...
                       'HessianFcn', 'objective', ...
                       'display', 'iter-detailed', ...
                       'MaxIterations', 100, ...
                       'StepTolerance', 1e-5, ...
                       'FunctionTolerance', 1e-5);
lambda = linspace(0, 1, 2);
nlambda = length(lambda);
duration = zeros(exp.nexperiments);
location_estimates = zeros(exp.nexperiments, nlambda, 2);
optim_results = zeros(exp.nexperiments, nlambda, 5);
done = zeros(exp.nexperiments, 1);
start_time = time();

for test_case=1:randi(771, 5) %exp.nexperiments
    tstart = tic;
    done(test_case) = 1;
    percent_done = sum(done(:))*100/numel(done);
    logging('Case: %s [%%%s] is starting...', test_case, round(percent_done, 2));
    for lambda_idx=1:length(lambda)
        lambda_i = lambda(lambda_idx);
        weights = [lambda_i, 1-lambda_i];
        fun = @(x) fun_extra(x, weights, exp.e(test_case, :), exp.nsamples, sqrt(exp.var_zeta(test_case, :)), L, f, g, h, exp.gt(test_case, :), true);
        p0 = [exp.e(test_case, :), 1/sum(exp.e(test_case, :))];
        [p_opt, fval, flag, output] = fminunc(fun, p0, options);
        optim_results(test_case, lambda_idx, :) = p_opt;
        et = p_opt(1:exp.nsensors);
        K = diag([p_opt(exp.nsensors+1), p_opt(exp.nsensors+1)]);
        cov_zeta = diag(exp.var_zeta(test_case, :));
        mu_e = et' + exp.nsamples * diag(cov_zeta);
        cov_e = 4 * diag(et) * cov_zeta' + 2 * exp.nsamples * (cov_zeta * cov_zeta');
        mu_x = (K * L * mu_e)';
        cov_x = K * L * cov_e * L' * K';
        det(cov_x)
        location_estimates(test_case, lambda_idx, :) = mu_x;
    end
    duration(test_case) = toc(tstart);
    eta = start_time + seconds(sum(duration(:))/percent_done*100);
    logging('Case: %s [%%%s] is done in %s seconds; ETA: %s...', test_case, round(percent_done, 2), duration(test_case), eta);
end

save('data/long_run.mat');
%     figure('Visible', 'off');
%     for sensor_idx = 1:exp.nsensors
%         mu = mu_e(sensor_idx);
%         sigma =  sqrt(cov_e(sensor_idx, sensor_idx));
%         x = norminv([0.01, .999], mu, sigma);
%         x = linspace(x(1), x(2), 100);
%         y = normpdf(x, mu, sigma); 
%         subplot(1,4,sensor_idx);
%         plot(x, y, 'k-'); hold on;
%         plot([e(test_case, sensor_idx), e(test_case, sensor_idx)], [0, max(y)], 'r-.'); hold off;
%         grid on; grid minor;
%     end
%     exportgraphics(gcf, ['figures/semi-results/energy_', num2str(test_case), '.png'], 'Resolution', 300, 'BackgroundColor', 'none');
%     figure('Visible', 'off');
%     rectangle('Position', [0 0 60 60], 'EdgeColor', [0 0 0], 'FaceColor', [0.7 0.7 0.7]); hold on;
%     scatter(L(1, :), L(2, :), 'kx');
%     % error_ellipse('mu', mu_x, 'C', cov_x, 'conf', 0.95);
%     scatter(mu_x(1), mu_x(2), 'r.')
%     scatter(x_heur(test_case, 1), x_heur(test_case, 2), 'gx')
%     scatter(exp.gt(test_case, 1), exp.gt(test_case, 2), 'bx')
    
%     axis equal; grid on; grid minor;
%     xlim([-10 70]); ylim([-10 70]);
%     exportgraphics(gcf, ['figures/semi-results/localization', num2str(test_case), '.png'], 'Resolution', 300, 'BackgroundColor', 'none');
%     close all;
% end

% x_semi = zeros(771, 2);
% for test_case=1:exp.nexperiments
%     et = results(test_case, 1:exp.nsensors);
%     K = diag(results(test_case, exp.nsensors+1:exp.nsensors+2));
%     cov_zeta = diag(exp.var_zeta(test_case, :));
%     mu_e = et' + exp.nsamples * diag(cov_zeta);
%     cov_e = 4 * diag(et) * cov_zeta' + 2 * exp.nsamples * (cov_zeta * cov_zeta');
%     x_semi(test_case, :) = (K * L * mu_e)';
% end
% error_x_semi = x_semi - exp.gt;
% error_x_semi_norm = vecnorm(error_x_semi, 2, 2);

function [f, g, h] = fun_extra(x, lambda, e, n, sigma_zeta, L, f, g, h, gt, plot)
    m = length(sigma_zeta);
    e_t = x(1:m);
    k_x = x(m+1);
    % k_y = x(m+2);
    K = diag([k_x, k_x]);
    cov_zeta = diag(sigma_zeta.^2);
    mu_e = e_t + n * diag(cov_zeta)';
    cov_e = (4 * diag(e_t) + 2 * n * cov_zeta) * cov_zeta;
    mu_x = diag([k_x, k_x])*L*mu_e';
    cov_x = K * L * cov_e * L' * K';

    f = subs(f, {"n", "e1", "e2", "e3", "e4", ...
                "e_t1", "e_t2", "e_t3", "e_t4", ...
                "sigma_zeta1", "sigma_zeta2", "sigma_zeta3", "sigma_zeta4", ...
                "lambda1", "lambda2", ...
                "x1", "x2", "x3", "x4", ...
                "y1", "y2", "y3", "y4", ...
                "k"}, ...
                {n, e(1), e(2), e(3), e(4), ...
                e_t(1), e_t(2), e_t(3), e_t(4), ...
                sigma_zeta(1), sigma_zeta(2), sigma_zeta(3), sigma_zeta(4), ...
                lambda(1), lambda(2), ...
                L(1, 1), L(1, 2), L(1, 3), L(1, 4), ...
                L(2, 1), L(2, 2), L(2, 3), L(2, 4), ...
                k_x});
    f = double(f);
    g = subs(g, {"n", "e1", "e2", "e3", "e4", ...
                "e_t1", "e_t2", "e_t3", "e_t4", ...
                "sigma_zeta1", "sigma_zeta2", "sigma_zeta3", "sigma_zeta4", ...
                "lambda1", "lambda2", ...
                "x1", "x2", "x3", "x4", ...
                "y1", "y2", "y3", "y4", ...
                "k"}, ...
                {n, e(1), e(2), e(3), e(4), ...
                e_t(1), e_t(2), e_t(3), e_t(4), ...
                sigma_zeta(1), sigma_zeta(2), sigma_zeta(3), sigma_zeta(4), ...
                lambda(1), lambda(2), ...
                L(1, 1), L(1, 2), L(1, 3), L(1, 4), ...
                L(2, 1), L(2, 2), L(2, 3), L(2, 4), ...
                k_x});

    g = double(g);
    h = subs(h, {"n", "e1", "e2", "e3", "e4", ...
                "e_t1", "e_t2", "e_t3", "e_t4", ...
                "sigma_zeta1", "sigma_zeta2", "sigma_zeta3", "sigma_zeta4", ...
                "lambda1", "lambda2", ...
                "x1", "x2", "x3", "x4", ...
                "y1", "y2", "y3", "y4", ...
                "k"}, ...
                {n, e(1), e(2), e(3), e(4), ...
                e_t(1), e_t(2), e_t(3), e_t(4), ...
                sigma_zeta(1), sigma_zeta(2), sigma_zeta(3), sigma_zeta(4), ...
                lambda(1), lambda(2), ...
                L(1, 1), L(1, 2), L(1, 3), L(1, 4), ...
                L(2, 1), L(2, 2), L(2, 3), L(2, 4), ...
                k_x});
    h = double(h);
    if plot
        figure(666);
        rectangle('Position', [0 0 30 30], 'EdgeColor', [0 0 0], 'FaceColor', [0.7 0.7 0.7]); hold on;
        scatter(L(1, :), L(2, :), 'kx');
        scatter(mu_x(1), mu_x(2), 'r.')
        scatter(gt(1), gt(2), 'gx')
        title(num2str(round([mu_x', log10(k_x)], 2)))
        axis equal; grid on; grid minor;
        xlim([-10 40]); ylim([-10 40]); drawnow;
    end
end
