clc; clearvars; close all; prepare_generic;
syms lambda real;
syms x_energy x_cov x_t [2 1] real;

assumeAlso(lambda>=0 & lambda<=1);

g = lambda * x_energy + (1-lambda) * x_cov
cost = expand(norm(x_t - g)^2);
dcost_dlambda = simplify_auto(diff(cost, lambda), true, 1000);

sol = solve(dcost_dlambda, lambda, "Real", true, "ReturnConditions", true);

pretty(collect(simplify(sol.lambda), [x_t1; x_t2]))

save('lambda.mat', "sol");
