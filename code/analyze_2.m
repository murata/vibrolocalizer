clc; clearvars; close all; clc;
load('data/windowed_heuristic.mat')
load('data/windowed_proposed.mat')
load('data/semi.mat')
exp = load('data/katsidimas_etal_organized.mat');
L = exp.sensor_locations';

e_semi = location_estimates - exp.gt;
e_semi_norm = vecnorm(e_semi, 2, 2);

x_heur = L * transpose(exp.e ./ sum(exp.e, 2));
x_heur = x_heur';

e_heur = x_heur - exp.gt;
e_heur_norm = vecnorm(e_heur, 2, 2);

e_heur_win = x_heur_win - exp.gt;
e_heur_win_norm = vecnorm(e_heur_win, 2, 2);

e_semi_win = x_semi_win - exp.gt;
e_semi_win_norm = vecnorm(e_semi_win, 2, 2);

describe(e_semi_norm)
describe(e_semi_win_norm)
describe(e_heur_norm)
describe(e_heur_win_norm)

e_space = linspace(0, 40, 200);
[~, e_pdf_heur] = epdf(e_heur_norm, e_space);
[~, e_pdf_semi] = epdf(e_semi_norm, e_space);
[~, e_pdf_heur_win] = epdf(e_heur_win_norm, e_space);
[~, e_pdf_semi_win] = epdf(e_semi_win_norm, e_space);

figure(1);
plot(e_space, cumtrapz(e_space, e_pdf_semi), 'k-', ...
     e_space, cumtrapz(e_space, e_pdf_semi_win), 'g--', ...
     e_space, cumtrapz(e_space, e_pdf_heur), 'r-', ...
     e_space, cumtrapz(e_space, e_pdf_heur_win), 'b--', 'Linewidth', 2);
grid on; grid minor;
ylim([0, 1.4]);
title("ECDF of Normed Localization Error");
xlabel("Error");
ylabel("C. Probability");
legend(["Proposed", "Proposed w/ Tight Window", "Heuristic", "Heuritic w/ Tight Window"]);

figure(2);
scatter(e_semi(:, 1), e_semi(:, 2), 'kx'); hold on;
scatter(e_semi_win(:, 1), e_semi_win(:, 2), 'g^');
scatter(e_heur(:, 1), e_heur(:, 2), 'rx');
scatter(e_heur_win(:, 1), e_heur_win(:, 2), 'b^');

axis equal; grid on; grid minor;
legend(["Proposed", "Proposed w/ Tight Window", "Heuristic", "Heuritic w/ Tight Window"]);
ylabel("Error in y- direction");
xlabel("Error in x- direction");
title("Localization Errors")
