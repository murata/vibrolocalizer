clc; close all; clearvars; prepare_generic;
folder = "data/data-lacrose-iepe";
runs = 1:5;
quarts = 1:4;

for run=runs
    for quart=quarts
        folder_name = sprintf("%s/run-%d/data_lacrosse-iepe-q%d", folder, run, quart);
        fname = "accels-*.mat";
        files_found = dir(sprintf("%s/%s", folder_name, fname));
        files_found = natsortfiles(files_found);
        stitched_data = zeros(length(files_found), 102400, 9);
        stitched_time = zeros(length(files_found), 102400);
        for file_idx=1:length(files_found)
            mat_file = sprintf("%s/%s", files_found(file_idx).folder, files_found(file_idx).name);
            data = load(mat_file);
            stitched_data(file_idx, :, :) = data.data;
            stitched_time(file_idx, :) = data.t;
        end
        fout_name = sprintf("%s/stitched.mat", folder_name);
        save(fout_name, "stitched_data", "stitched_time")
    end
end
