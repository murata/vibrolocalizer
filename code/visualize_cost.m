
figure(1);
subplot(131);
contourf(a(:, :, 1), b(:, :, 1), J1(:, :, 1), 'edgecolor', 'none');
subtitle('$J1$'); xlabel('$e_{t1}$'); ylabel('$e_{t2}$'); axis equal;
subplot(132);
contourf(a(:, :, 1), b(:, :, 1), J2(:, :, 1), 'edgecolor', 'none');
subtitle('$J2$'); xlabel('$e_{t1}$'); ylabel('$e_{t2}$'); axis equal;
subplot(133);
contourf(a(:, :, 1), b(:, :, 1), J1(:, :, 1) + J2(:, :, 1), 'edgecolor', 'none');
subtitle('$J1 + J2$'); xlabel('$e_{t1}$'); ylabel('$e_{t2}$'); axis equal; colormap jet; 
