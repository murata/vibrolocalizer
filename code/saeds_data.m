clc; clearvars; close all; prepare_generic;
load("data/GroundTruth_SensorCoordinates_DetectionTime.mat")
load("data/AI_loc_Subject1SD_8192Hz_2to500Hz.mat");
experiment1.nsensors = 11;
experiment1.nsteps = 162;
experiment1.fs = 8092;
experiment1.t = (1:size(data, 1))/experiment1.fs;
experiment1.sensor_locations = sensorCoordinates;
experiment1.sigma_zeta = 1e-5;
experiment1.groundtruth = trueFootstepLocations;
experiment1.steps = struct();

for step_idx = 1:162
    try
        [~, min_id_start] = min(abs(experiment1.t-approximateFootstepDetectionTime(step_idx)));
        [~, min_id_end] = min(abs(experiment1.t-approximateFootstepDetectionTime(step_idx+1)));
    catch exception
        if strcmp("MATLAB:badsubscript", exception.identifier)
            min_id_end = size(data, 1);
        end
    end
    experiment1.steps(step_idx).nsamples = min_id_end - min_id_start;
    experiment1.steps(step_idx).z = data(min_id_start:min_id_end, :);
    experiment1.steps(step_idx).e = energy(data(min_id_start:min_id_end, :), 1);
end
clear data min_id_start min_id_end step_idx exception;
load("data/AI_loc_Subject2Rodri_8192Hz_2to500Hz.mat");
experiment2.nsensors = 11;
experiment2.nsteps = 162;
experiment2.fs = 8092;
experiment2.t = (1:size(data, 1))/experiment2.fs;
experiment2.sensor_locations = sensorCoordinates;
experiment2.sigma_zeta = 1e-3;
experiment2.groundtruth = trueFootstepLocations;
experiment2.steps = struct();

for step_idx = 1:162
    try
        [~, min_id_start] = min(abs(experiment2.t-approximateFootstepDetectionTime(step_idx)));
        [~, min_id_end] = min(abs(experiment2.t-approximateFootstepDetectionTime(step_idx+1)));
    catch exception
        if strcmp("MATLAB:badsubscript", exception.identifier)
            min_id_end = size(data, 1);
        end
    end
    experiment2.steps(step_idx).nsamples = min_id_end - min_id_start;
    experiment2.steps(step_idx).z = data(min_id_start:min_id_end, :);
    experiment2.steps(step_idx).e = energy(data(min_id_start:min_id_end, :), 1);
end
clear data min_id_start min_id_end step_idx exception;

save('data/goodwin_step_data.mat', 'experiment1', 'experiment2');
