clc; clearvars; close all; prepare_generic;
m = 4;
syms e e_t sigma_zeta [m, 1] real;
syms n real;
syms x y [m 1] real;
% syms K [m m] real;
syms lambda [2 1] real;
L = [x'; y'];
% assumeAlso(sum(lambda)==1);
assumeAlso(e>0);
assumeAlso(e_t>=0);
assumeAlso(sigma_zeta>0);
assumeAlso(n>0);

cov_zeta = diag(sigma_zeta.^2);
mean_e = e_t + n * diag(cov_zeta);
cov_e = (4 * diag(e_t) + 2 * n * cov_zeta) * cov_zeta';

mean_x  = L * mean_e / norm(e_t, 1);
cov_x = L * cov_e * L' / norm(e_t, 1)^2;

J_inner = simplify([(e-mean_e)'*inv(cov_e)*(e-mean_e);
                    det(cov_x)]);
J = lambda' * J_inner;
%% 
del = gradient(J, e_t);
% simplify_auto(del, false, 1000)
save("data/del_simplified.mat", "del", "J");

