clc; close all; prepare_generic;
exp = load('data/katsidimas_etal_organized.mat');
[~, t_end_id] = min(abs(exp.t-2.5e-3));
[~, t_start_id] = min(abs(exp.t-2e-3));
L = exp.sensor_locations';
% figure;
% for i=1:exp.nexperiments
%     plot(exp.t(t_start_id:t_end_id), squeeze(exp.z_normalized(i, :, t_start_id:t_end_id)));
% end
% grid on; grid minor;

z = exp.z_normalized(:, :, t_start_id:t_end_id);
e = energy(z, 3);
% ek = cumsum(exp.z_normalized.^2, 3);
% plot(exp.t, squeeze(ek(15, :, :)));
% grid on; grid minor;
% xlabel('Time [sec]'); ylabel("$\sum\limits_{i=0}^k e[i]$");
% title("Energy Accumulation Graph");
% figure(2);
% plot(exp.t, squeeze(exp.z_normalized(15, :, :)));
% xlabel("Time [sec]");
% ylabel("Measured Signal");
% grid on; grid minor;
% xlim([0 1e-2])
x_heur_win = L * transpose(e ./ sum(e, 2));
x_heur_win = x_heur_win';

e_heur_win = x_heur_win - exp.gt;
e_heur_norm_win = vecnorm(e_heur_win, 2, 2);
nsamples = t_end_id - t_start_id;
save('data/windowed_heuristic.mat', 'x_heur_win')
options = optimoptions('fminunc', ...
                       'Algorithm','trust-region', ...
                       'SpecifyObjectiveGradient',true, ...
                       'HessianFcn', 'objective', ...
                       'display', 'notify-detailed', ...
                       'MaxIterations', 200, ...
                       'StepTolerance', 1e-5, ...
                       'FunctionTolerance', 1e-5);
                       
parfor test_case=1:exp.nexperiments
    % tstart = tic;
    % done(test_case) = 1;
    % percent_done = sum(done(:))*100/numel(done);
    % logging('Case: %s [%%%s] is starting...', test_case, round(percent_done, 2));
    logging('Case: %s is starting...', test_case);
    fun = @(x) fun_extra(x, e(test_case, :), nsamples, 1/sum(e(test_case, :)), sqrt(exp.var_zeta(test_case, :)), L, f, g, h, false);
    [p_opt, fval, flag, output] = fminunc(fun, exp.e(test_case, :), options);
    optim_results(test_case, :) = p_opt;
    et = p_opt(1:exp.nsensors);
    cov_zeta = diag(exp.var_zeta(test_case, :));
    K = diag([1/sum(et), 1/sum(et)]);
    % mu_e = et' + exp.nsamples * diag(cov_zeta);
    cov_e = 4 * diag(et) * cov_zeta' + 2 * exp.nsamples * (cov_zeta * cov_zeta');
    % mu_x = (K * L * mu_e)';
    cov_x = K * L * cov_e * L' * K';
    location_estimates(test_case, :) = K * L * et';
    % duration(test_case) = toc(tstart);
    % eta = start_time + seconds(sum(duration(:))/percent_done*100);
    % logging('Case: %s [%%%s] is done in %s seconds; ETA: %s...', test_case, round(percent_done, 2), duration(test_case), eta);
end
x_semi_win = location_estimates;
save('data/windowed_proposed.mat', 'x_semi_win');

function [f, g, h] = fun_extra(x, e, n, k, sigma_zeta, L, f, g, h, plot)
    m = length(sigma_zeta);
    e_t = x(1:m);
    K = diag([k, k]);
    cov_zeta = diag(sigma_zeta.^2);
    mu_e = e_t + n * diag(cov_zeta)';
    cov_e = (4 * diag(e_t) + 2 * n * cov_zeta) * cov_zeta;
    mu_x = K * L * mu_e';
    cov_x = K * L * cov_e * L' * K';

    f = subs(f, {"n", "e1", "e2", "e3", "e4", ...
                "e_t1", "e_t2", "e_t3", "e_t4", ...
                "sigma_zeta1", "sigma_zeta2", "sigma_zeta3", "sigma_zeta4", ...
                "x1", "x2", "x3", "x4", ...
                "y1", "y2", "y3", "y4"}, ...
                {n, e(1), e(2), e(3), e(4), ...
                e_t(1), e_t(2), e_t(3), e_t(4), ...
                sigma_zeta(1), sigma_zeta(2), sigma_zeta(3), sigma_zeta(4), ...
                L(1, 1), L(1, 2), L(1, 3), L(1, 4), ...
                L(2, 1), L(2, 2), L(2, 3), L(2, 4)});
    f = double(f);
    g = subs(g, {"n", "e1", "e2", "e3", "e4", ...
                "e_t1", "e_t2", "e_t3", "e_t4", ...
                "sigma_zeta1", "sigma_zeta2", "sigma_zeta3", "sigma_zeta4", ...
                "x1", "x2", "x3", "x4", ...
                "y1", "y2", "y3", "y4"}, ...
                {n, e(1), e(2), e(3), e(4), ...
                e_t(1), e_t(2), e_t(3), e_t(4), ...
                sigma_zeta(1), sigma_zeta(2), sigma_zeta(3), sigma_zeta(4), ...
                L(1, 1), L(1, 2), L(1, 3), L(1, 4), ...
                L(2, 1), L(2, 2), L(2, 3), L(2, 4)});

    g = double(g);
    h = subs(h, {"n", "e1", "e2", "e3", "e4", ...
                "e_t1", "e_t2", "e_t3", "e_t4", ...
                "sigma_zeta1", "sigma_zeta2", "sigma_zeta3", "sigma_zeta4", ...
                "x1", "x2", "x3", "x4", ...
                "y1", "y2", "y3", "y4"}, ...
                {n, e(1), e(2), e(3), e(4), ...
                e_t(1), e_t(2), e_t(3), e_t(4), ...
                sigma_zeta(1), sigma_zeta(2), sigma_zeta(3), sigma_zeta(4), ...
                L(1, 1), L(1, 2), L(1, 3), L(1, 4), ...
                L(2, 1), L(2, 2), L(2, 3), L(2, 4)});
    h = double(h);
    if plot
        figure(666);
        rectangle('Position', [0 0 60 60], 'EdgeColor', [0 0 0], 'FaceColor', [0.7 0.7 0.7]); hold on;
        scatter(L(1, :), L(2, :), 'kx');
        scatter(mu_x(1), mu_x(2), 'r.')
        title(num2str(round([mu_x', k_x, k_y], 2)))
        axis equal; grid on; grid minor;
        xlim([-10 70]); ylim([-10 70]); drawnow;
    end
end
